export class UserPhoto {

  // Objet photo fourni par la méthode getphoto() de la caméra
  // https://capacitorjs.com/docs/apis/camera#photo

  // chemin spécifique à la plateforme utilisé pouvant être
  // lu par l'API filesystem
  filepath: string;
  // chemin qui peut être utilisé pour définir l’attribut src
  // d’une image pour un chargement et un rendu efficaces.
  webViewPath: string;

}
