import { Injectable } from '@angular/core';

// ajout des imports pour l'utilisation de la caméra, du systeme de fichier et du stockage
import { Camera, CameraResultType, CameraSource, Photo } from '@capacitor/camera';
import { Filesystem, Directory } from '@capacitor/filesystem';
import { Storage } from '@capacitor/storage';
import { UserPhoto } from '../models/user-photo';

// importation de l'API Platform pour la gestion du type d'appareil (PWA, Mobile)
import { Platform } from '@ionic/angular';
import { Capacitor } from '@capacitor/core';

@Injectable({
  providedIn: 'root'
})
export class PhotoService {

  // définition d'un tableau de Photos
  public photos: UserPhoto[] = [];
  // Declaration d'une constante définissant notre clé de stockage
  private PHOTO_STORAGE: string = 'photos';
  // variable pour gestion platform
  private platform : Platform;

  constructor(platform : Platform) {
      // récuperation des informations sur la platform en cours à l'appel du composant
      this.platform = platform;
   }

  /**
   * Methode d'ajout photo à la gallerie
   */
  public async addNewToGallery() {
    // utilisation de la méthode de Capacitor Camera.getPhoto()
    // pas de platform à spécifier, ni de codes spécifiques.
    // Capacitor fait abstraction des platformes
    // https://capacitorjs.com/docs/v2/apis/camera#getphoto
    // type de résultat ( uri | base64 | DataUrl )
    // source (camera | photos | prompt)
    // qualité as JPEG 0-100

    /**
     * Methode de capture de photos dans un objet capturedPhoto retourné par getPhoto()
     * getPhoto(options: ImageOptions) => Promise<Photo>
     */
    const capturedPhoto = await Camera.getPhoto({
      resultType: CameraResultType.Uri,             // utilisation du systeme de fichiers
      source: CameraSource.Camera,                  // utilisation de la caméra
      quality: 100                                  // qualité de la photo
    });

    // Sauvegarde de la photo
    const saveImageFile = await this.savePicture(capturedPhoto);

    // ajout de la photo capturé dans le tableau
          // --- avant sauvegarde
          // this.photos.unshift({
          //   filepath: "...soon...",
          //   webViewPath: capturedPhoto.webPath
          // });
    // --- apres sauvegarde
    this.photos.unshift(saveImageFile);

    // Appel à Storage.set() pour stocker le tableau de photos
    Storage.set({
      key: this.PHOTO_STORAGE,
      value: JSON.stringify(this.photos),
    });
  }

  /**
   * Methode de sauvegarde de la photo
   * @param photo
   * @returns
   */
  private async savePicture(photo : Photo) {

    const base64Data = await this.readAsBase64(photo);  // convertion au format base64 demandé par API FileSystem

    // Ecriture du fichier dans le Directory            // https://capacitorjs.com/docs/apis/filesystem#directory
    const fileName = new Date().getTime() + '.jpeg';    // création du nom du fichier
    const savedFile = await Filesystem.writeFile({
      path: fileName,                                   // le chemin avec le nom du fichier à écrire
      data: base64Data,                                 // la donnée à ecrire au format Base64
      directory: Directory.Data                         // Le repertoire où on écrit le fichier
    });

    // // Prise en compte Mobile
    if (this.platform.is("hybrid")) {
      // affiche l'image en réécrivant le 'file://' en HTTP
      // Details: https://ionicframework.com/docs/building/webview#file-protocol
      return {
        filepath: savedFile.uri,
        webViewPath: Capacitor.convertFileSrc(savedFile.uri),
      };
    }
    // prise en compte PWA
    else {
      // Utilisez webPath pour afficher la nouvelle image au lieu de base64 puisqu’elle est
      // déjà chargé en mémoire
      return {
        filepath: fileName,
        webViewPath: photo.webPath
      };
    }
  }

  /**
   * Methode de chargement des photos
   */
   public async loadSaved() {
    // récuperation de la liste des photos stockée dans le Storage
    const photoList = await Storage.get({ key: this.PHOTO_STORAGE });
    // on recré le tableau de nos photos
    this.photos = JSON.parse(photoList.value) || [];

    // // Prise en compte PWA seulement car avec le mobile on peut adresse directement
    // // la source à l'image <img src="x" />
    if (!this.platform.is("hybrid")) {
      // affichage de la photo en la lisant dans le format base64
      for (let photo of this.photos) {
        // lecture de chaque photo sauvegardé dans le file system
        const readFile = await Filesystem.readFile({
          path: photo.filepath,
          directory: Directory.Data,
        });

        // Pour la plateforme web seulement, chargement de la photo comme une donnée base64
        photo.webViewPath = `data:image/jpeg;base64,${readFile.data}`;
      }
    }
  }

  /**
   * Methode de conversion de la photo en base 64
   * @param photo
   * @returns
   */
  private async readAsBase64(photo: Photo) {

    // // Hybrid detectera soit Cordova ou Capacitor => MOBILE
    if (this.platform.is('hybrid')) {
      // lecture du fichier au format base64
      const file = await Filesystem.readFile({
        path: photo.path
      });
      return file.data;
    }
    // // Pour les PWA
    else {
      // Fetch la photo, lire en blob, alors convertir en format base64
      // recupere la ressource webPath de Photo en un objet de type Response
      // il s'agit d'un objet HTTP pas d'une image.
      const response = await fetch(photo.webPath!);
      // Pour extraire notre image de la réponse, utilisation de la méthode blob()
      // construction d'un objet blob dont l'interface File est basée
      const blob = await response.blob();
      // conversion du blob en base64
      // le blob (Binary Large Objetcs) représente un objet semblable à un fichier
      // qui contient des données brutes mais qui ne sont pas dans un format javascript
      return await this.convertBlobToBase64(blob) as string;
    }
  }

  /**
   * Methode de conversion du Blob en Base64
   * @param blob
   * @returns
   */
  private convertBlobToBase64 = (blob: Blob) => new Promise((resolve, reject) => {
    // appel de l'API FileReader pour lire le contenu du blob de maniere asynchrone
    const reader = new FileReader();
    // Sur erreur, rejet de la promesse
    reader.onerror = reject;
    // Sur success, on récupere notre contenu
    reader.onload = () => {
        resolve(reader.result);
    };
    // lancement de la lecture du blob et qui contiendra dans result les données du fichier sous forme de chaine de caracteres
    reader.readAsDataURL(blob);
  });
}
