import { Component } from '@angular/core';
import { PhotoService } from '../services/photo.service'

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  // appel du service au travers du contructor
  constructor(public photoService : PhotoService) {}

  async ngOnInit() {
    await this.photoService.loadSaved();
  }

  /**
   * Méthode d'ajout d'une photo dans la gallerie qui agit sur clic du bouton caméra
   */
  addPhotoToGallery() {
    this.photoService.addNewToGallery();
  }
}
